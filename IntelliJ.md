IntelliJ
======
`Intelligent Java IDE - 智能的 Java 集成開發環境`

集成開發環境`IDE`
------
`IDE , Integrated Development Environment`

+ Eclipse `免費，功能最多樣`
+ IntelliJ `針對 Java 的集成開發環境`
  + 社群版 : 免費 `JavaSE`
  + 企業版 : 付費 `JavaEE`


### IntelliJ
+ 官網 : https://www.jetbrains.com/idea/
+ 下載 : https://www.jetbrains.com/idea/download/


### 教學影片
+ [`IntelliJ IDEA 入門指南 : Java 開發者的神兵利器`](https://youtu.be/FkL17L_gokc)


<br>


一般專案
------
`New Project > Java`


### Sample Code

`com/enoxs/AppMain.java`
```java
package com.enoxs;
public class AppMain {
    public static void main(String[] args) {
        System.out.println("Hello World");
    }
}
```


**Source Code**
+ <https://gitlab.com/GammaRayStudio/Tutor/JavaSE.git>
```
/HelloWorld
```

<br>

單元測試框架 `JUnit`
------
### 設置

**Modules**
+ test/

**Libraries**
+ lib/hamcrest-core-1.3.jar
+ lib/junit-4.12.jar

### 操作
+ 快捷鍵 : cmd + shift + T `Go to Test`

<br>

### Sample Code
`test/com/enoxs/AppMainTest.java`

```java
package com.enoxs;
import org.junit.Test;
public class AppMainTest {

    @Test
    public void helloWorld() {
        System.out.println("Hello World");
    }

    @Test
    public void printMsg() {
        System.out.println("Show Message.");
    }

}
```


<br>


Gradle 專案
------
`New Project > Gradle`

### 主流專案建構工具
+ Ant `古老，腳本`
+ Maven `XML，常見`
+ Gradle `較新，Android 專案`

### 專案結構
+ build.gradle
+ src/
  + main/
    + java/
    + resource/
  + test/
    + java/
    + resource/

### build.gradle
`初始化，已經預設 JUnit 5 框架`
```
dependencies {
    testImplementation 'org.junit.jupiter:junit-jupiter-api:5.6.0'
    testRuntimeOnly 'org.junit.jupiter:junit-jupiter-engine'
}
```

<br>

### Sample Code
**AppMain.java**
```java
package com.enoxs;
public class AppMain {
    public static void main(String[] args) {
        System.out.println("Hello JavaProjSE - v1.0.1");
    }
}
```

**AppMainTest.java**
```java
package com.enoxs;
import org.junit.jupiter.api.Test;
class AppMainTest {
    @Test
    public void helloWorld(){
        System.out.println("Hello World");
    }
    @Test
    public void printMsg(){
        System.out.println("Show Message.");
    }
}
```

**Source Code**
+ <https://gitlab.com/GammaRayStudio/Tutor/JavaSE.git>
```
/JavaProjSE
```

<br>

### Main 模組下執行單元測試
`Preferences > Build,Execution,Deloyment > Build Tools > Gradle`

+ Build and run using : `IntelliJ`
+ Run tests using : `IntelliJ`

<br>

說明 Gradle 專案與 JUnit 框架的原因
-------
```
Gradle 專案是未來的主流架構，
JUnit 框架則是工程師在測試新的工具或新的語法時，一個好用且可以快速了解特性的方法。

後續的 Java 語法講解，都會直接使用 JUnit 框架來示範與說明。
```



