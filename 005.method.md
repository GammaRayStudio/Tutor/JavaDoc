方法
======
`Method`

函式
------
`function`

    f(x) = 2x + y

    input : x = 2 , y = 1
    output : f(x) = 5

```c
int cal2XPlusY(int a , int b){
  int c = 0;
  c = 2a + b;
  return c;
}
```

**C 語言**

   當敘述的計算重複出現，透過函式來減少重複代碼

<br>


方法
------
`Method`

+ 相同 : 可重複呼叫
+ 不同 : 物件導向，代表該類別的特有行為

<br>

```java
public int calSum(int a , int b){
    return a + b;
}
```

+ public : 修飾詞
+ int : 回傳型態
+ calSum : 方法名稱
+ int a , int b : 參數名稱與型態
+ return : 回傳值

<br>

**語法**
```java
修飾詞 回傳型態 方法名稱 (參數列表){
    陳述動作
}
```

+ 修飾詞 : private / public / protect
+ 回傳型態 : void / int / char / float . . .
+ 參數列表 : empty / int / char / float . . . | 區域變數
+ 陳述動作 : 依據回傳型態決定是否有回傳值

<br>

void
```java
public void printMsg(){
    System.out.println("Message");
}

@Test
public void testPrintMsg(){
    printMsg();
}
```

int
```java
public int calSum(int a , int b){
    return a + b;
}

@Test
public void testCalSum(){
    int a = 10;
    int b = 20;
    int c = 0;
    c = calSum(a , b);
    System.out.println("a + b = " + c);
}
```

boolean
```java
public boolean isLargeThanTen(int num){
    return num > 10 ? true : false;
}

@Test
public void testIsLargeThen(){
    int num = 15;

    boolean isLarge = isLargeThanTen(num);

    System.out.println("number > 10 ? => " + isLarge);
}
```

<br>

**JUnit 單元測試框架的測試單位**

Success :
```java
@Test
public void testSucc(){
    int a = 10;
    int b = 20;
    int actual = calSum(a,b);
    int expect = 30;
    assertEquals(expect,actual);
}
```

Fail : 
```java
@Test
public void testFail(){
    int a = 10;
    int b = 20;
    int actual = calSum(a,b);
    int expect = 20;
    assertEquals(expect,actual);
}
```

<br>

變量參數
------
`var-args`

    JDK 1.5 後，允許將相同類型的參數傳遞給方法

```java
public int calTotal(int ... num){
    int result = 0;
    for(int i=0;i<num.length;i++){
        result += num[i];
        // result = result + num[i]
    }
    return result;
}

@Test
public void testCalTotal(){
    int [] number = {1,2,3,4,5,6,7,8,9,10};
    int result = calTotal(number);
    System.out.println("result => " + result);
}
```

<br>

遞迴
------
`Recursive`

### 迴圈
```java
for(int i;i<10;i++){
  // do something
}
```

+ 重複做十次

### 遞迴
```
f(x) = x / 2
input : x = f(100)
output : f(f(100)) = 25
```

    函式呼叫函式本身

計算 : 重複減半後，小於 50 的數
```java
public int calHalfLess50(int number){
    int result = number / 2;
    if (result > 50){
        result = calHalfLess50(result);
    }
    return result;
}

@Test
public void testCalHalfTotal(){
    int num = 120;
    int result = calHalfLess50(num);
    System.out.println("result => " + result);
}
```

計算 : 最大公因數
```java
public int calGCD(int a , int b){
    int result = 0;
    if(a % b == 0){
        result = b;
    }else{
        result = calGCD(b , a%b);
    }
    return result;
}

@Test
public void testCalGCD(){
    int num01 = 50;
    int num02 = 100;
    int result = calGCD(num01 , num02);
    System.out.println("result => " + result);
}
```



